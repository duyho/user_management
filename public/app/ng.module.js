var app = angular.module('userManagementApp', ['ngRoute']);

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});
app.config(function config($routeProvider) {
        $routeProvider.
        when('/', {
            template: '<user-component></user-component>'
        }).when('/roles', {
            template: '<role-component></role-component>'
        }).when('/permissions', {
            template: '<permission-component></permission-component>'
        }).when('/login', {
            template: '<login-component></login-component>'
        }).otherwise({redirectTo: '/'});
    }
);

app.factory('httpRequestInterceptor', function () {
    return {
        request: function (config) {
            var token = localStorage.getItem('token');
            config.headers['Authorization'] = 'Bearer ' + token;
            return config;
        }
    };
});

app.service('authInterceptor', function($rootScope, $q, $location) {
    var service = this;
    service.responseError = function(response) {
        var status = response.status;
        if (status == 401 || status == 403){
            if (status == 401) {
                localStorage.removeItem('token');
                localStorage.removeItem('authenticatedUser');
                $rootScope.$emit('updateAuthenticatedUser', {});
            }
            $location.path('/login').search({status: status});
        }
        return $q.reject(response);
    };
});


app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
    $httpProvider.interceptors.push('authInterceptor');
});

app.constant('config', {
    genders: {
        male: 1,
        female: 2
    },
});

