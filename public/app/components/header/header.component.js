app.component('headerComponent', {
    templateUrl: '/app/components/header/header.template.html',
    controller: function($scope, $rootScope, $http, $location) {
        var vm = this;

        function updateAuthenticatedUser() {
            $rootScope.authenticatedUser = JSON.parse(localStorage.getItem('authenticatedUser'));
        }
        updateAuthenticatedUser();

        $http.get('/api/auth/info').then(function(response) {
            localStorage.setItem('authenticatedUser', JSON.stringify(response.data));
            $rootScope.$emit('updateAuthenticatedUser', 'asf');
        });

        vm.logoutClickHandler = function() {
            localStorage.removeItem('authenticatedUser');
            localStorage.removeItem('token');
            updateAuthenticatedUser();
            $location.path('/login');
        };

        $rootScope.$on('updateAuthenticatedUser', function(e, data) {
            updateAuthenticatedUser();
        });
    }
});
