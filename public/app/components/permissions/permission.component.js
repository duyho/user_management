app.component('permissionComponent', {
    templateUrl: '/app/components/permissions/permission.template.html',
    controller: function($scope, $http, config) {
        $scope.permissions = [];

        $http.get('/api/permissions').then(function(response) {
            $scope.permissions = response.data;
        });

        $scope.actionType  = 'create';
        $scope.currentPermission = {};

        $scope.openCreateModal = function() {
            $scope.currentPermission = {};
            $scope.$broadcast('deliverNewPermission', {});
            $scope.actionType = 'create';
            $('#permissionDetailModal').modal();
        };
        $scope.openUpdateModal = function(index) {
            $scope.currentPermission = JSON.parse(JSON.stringify($scope.permissions[index]));
            $scope.$broadcast('deliverNewPermission', $scope.currentPermission);
            $scope.actionType = 'update';
            $('#permissionDetailModal').modal();
        };
        $scope.openDeletePopup = function(index) {
            var permission = $scope.permissions[index];
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this permission!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $http.delete(`/api/permissions/${permission.id}`).then(function() {
                            swal("Poof! The permission has been deleted!", {
                                icon: "success",
                            });
                            $scope.permissions.splice(index, 1);
                        });

                    } else {
                    }
                });
        };

        $scope.$on('permissionCreated', function(e, data) {
            $scope.permissions.push(data);
        });

        $scope.$on('permissionUpdated', function(e, data) {
            for (var index in $scope.permissions) {
                if ($scope.permissions[index].id == data.id) {
                    $scope.permissions[index] = data;
                }
            }
        })
    }
});
