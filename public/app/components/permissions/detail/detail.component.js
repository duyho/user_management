app.component('permissionDetailModal', {
    templateUrl: '/app/components/permissions/detail/detail.template.html',
    controller: function($scope, $http) {
        var vm = this;
        vm.validations = {};
        vm.submitHandler = function() {
            if(!$scope.permissionForm.$valid) {
                angular.forEach($scope.permissionForm.$error, function (field) {
                    angular.forEach(field, function(errorField){
                        errorField.$setTouched();
                    });
                });
                return;
            }

            $('#permissionDetailModal').modal('hide');

            if (vm.actionType == 'create') {
                $http.post('/api/permissions', vm.permission).then(function(response) {
                    $scope.$emit('permissionCreated', response.data);
                    swal("Poof! The permission has been created!", {
                        icon: "success",
                    });
                });
            }
            else {
                $http.put(`/api/permissions/${vm.permission.id}`, vm.permission).then(function(response) {
                    $scope.$emit('permissionUpdated', response.data);
                    swal("Poof! The permission has been updated!", {
                        icon: "success",
                    });
                });
            }
        };

        vm.cancelHandler = function() {
            var isChanged = false;
            if (JSON.stringify(vm.originalPermission) != JSON.stringify(vm.permission)) {
                isChanged = true;
            }

            if (isChanged) {
                swal({
                    title: "Are you sure?",
                    text: "Once canceled, you will not be able to recover this data!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willCancel) => {
                        if (!willCancel) {
                            $('#permissionDetailModal').modal();
                        }
                    });
            }
        };

        $scope.$on('deliverNewPermission', function(event, permission) {
            $scope.permissionForm.$setPristine();
            $scope.permissionForm.$setUntouched();

            if (typeof permission.id === 'undefined') {
                vm.actionType = 'create';
                vm.permission = {};
                vm.originalPermission = {};
            }
            else {
                vm.actionType = 'update';
                $http.get(`/api/permissions/${permission.id}`).then(function (response) {
                    vm.permission = response.data;
                    vm.originalPermission = JSON.parse(JSON.stringify(vm.permission));
                });
            }

        });
    },
    bindings: {
    }
});
