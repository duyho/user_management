app.component('roleDetailModal', {
    templateUrl: '/app/components/roles/detail/detail.template.html',
    controller: function($scope, $http) {
        var vm = this;
        vm.validations = {};
        vm.submitHandler = function() {
            if(!$scope.roleForm.$valid) {
                angular.forEach($scope.roleForm.$error, function (field) {
                    angular.forEach(field, function(errorField){
                        errorField.$setTouched();
                    });
                });
                return;
            }

            $('#roleDetailModal').modal('hide');

            if (vm.actionType == 'create') {
                $http.post('/api/roles', vm.role).then(function(response) {
                    $scope.$emit('roleCreated', response.data);
                    swal("Poof! The role has been created!", {
                        icon: "success",
                    });
                });
            }
            else {
                $http.put(`/api/roles/${vm.role.id}`, vm.role).then(function(response) {
                    $scope.$emit('roleUpdated', response.data);
                    swal("Poof! The role has been updated!", {
                        icon: "success",
                    });
                });
            }
        };

        vm.cancelHandler = function() {
            var isChanged = false;

            if (Object.keys(vm.originalRole).length > 0 && !vm.role.permissions)
                vm.role.permissions = [];

            if (JSON.stringify(vm.originalRole) != JSON.stringify(vm.role)) {
                isChanged = true;
            }

            if (isChanged) {
                swal({
                    title: "Are you sure?",
                    text: "Once canceled, you will not be able to recover this data!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willCancel) => {
                        if (!willCancel) {
                            $('#roleDetailModal').modal();
                        }
                    });
            }
        };

        function updatePermisisons() {
            $http.get('/api/permissions').then(function(response) {
                vm.permissions = response.data;
            });
        }

        $scope.$on('deliverNewRole', function(event, role) {
            $scope.roleForm.$setPristine();
            $scope.roleForm.$setUntouched();
            updatePermisisons();

            if (typeof role.id === 'undefined') {
                vm.actionType = 'create';
                vm.role = {};
                vm.originalRole = {};
            }
            else {
                vm.actionType = 'update';
                $http.get(`/api/roles/${role.id}`).then(function (response) {
                    vm.role = response.data;
                    vm.originalRole = JSON.parse(JSON.stringify(vm.role));
                });
            }

        });
    },
    bindings: {
    }
});
