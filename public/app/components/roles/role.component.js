app.component('roleComponent', {
    templateUrl: '/app/components/roles/role.template.html',
    controller: function($scope, $http, config) {
        $scope.roles = [];

        $http.get('/api/roles').then(function(response) {
            $scope.roles = response.data;
        });

        $scope.actionType  = 'create';
        $scope.currentRole = {};

        $scope.openCreateModal = function() {
            $scope.currentRole = {};
            $scope.$broadcast('deliverNewRole', {});
            $scope.actionType = 'create';
            $('#roleDetailModal').modal();
        };
        $scope.openUpdateModal = function(index) {
            $scope.currentRole = JSON.parse(JSON.stringify($scope.roles[index]));
            $scope.$broadcast('deliverNewRole', $scope.currentRole);
            $scope.actionType = 'update';
            $('#roleDetailModal').modal();
        };
        $scope.openDeletePopup = function(index) {
            var role = $scope.roles[index];
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this role!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $http.delete(`/api/roles/${role.id}`).then(function() {
                            swal("Poof! The role has been deleted!", {
                                icon: "success",
                            });
                            $scope.roles.splice(index, 1);
                        });

                    } else {
                    }
                });
        };

        $scope.$on('roleCreated', function(e, data) {
            $scope.roles.push(data);
        });

        $scope.$on('roleUpdated', function(e, data) {
            for (var index in $scope.roles) {
                if ($scope.roles[index].id == data.id) {
                    $scope.roles[index] = data;
                }
            }
        })
    },
});
