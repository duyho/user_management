app.component('loginComponent', {
    templateUrl: '/app/components/login/login.template.html',
    controller: function($scope, $rootScope, $http, $location, $routeParams) {
        var vm = this;
        vm.user = {};

        vm.errorMessage = "";

        vm.$onInit = function() {
            vm.errorMessage = "";
            if ($routeParams.status == 401) {
                vm.errorMessage = "Username and password does not match!";
            }
            else if ($routeParams.status == 403) {
                vm.errorMessage = 'Your access is denied!';
            }
        };

        vm.submitHandler = function(){
            $http.post('/api/login_check', vm.user).then(function(response) {
                localStorage.setItem('token', response.data.token);
                $http.get('/api/auth/info').then(function(response) {
                    localStorage.setItem('authenticatedUser', JSON.stringify(response.data));
                    $rootScope.$emit('updateAuthenticatedUser', 'asf');
                    $location.path('/');
                });

            });
        };
    }
});
