app.component('userDetailModal', {
    templateUrl: '/app/components/users/detail/detail.template.html',
    controller: function($scope, $http, config) {
        var vm = this;
        vm.constants = config;
        vm.changePassword = false;
        vm.validations = {};
        vm.submitHandler = function() {
            if(!$scope.userForm.$valid) {
                angular.forEach($scope.userForm.$error, function (field) {
                    angular.forEach(field, function(errorField){
                        errorField.$setTouched();
                    });
                });
                return;
            }

            $('#userDetailModal').modal('hide');

            if (vm.actionType == 'create') {
                $http.post('/api/users', vm.user).then(function(response) {
                    $scope.$emit('userCreated', response.data);
                    swal("Poof! The user has been created!", {
                        icon: "success",
                    });
                });
            }
            else {
                $http.put(`/api/users/${vm.user.id}`, vm.user).then(function(response) {
                    $scope.$emit('userUpdated', response.data);
                    swal("Poof! The user has been updated!", {
                        icon: "success",
                    });
                });
            }
        };

        vm.cancelHandler = function() {
            var isChanged = false;
            if (JSON.stringify(vm.originalUser) != JSON.stringify(vm.user)) {
                isChanged = true;
            }

            if (isChanged) {
                swal({
                    title: "Are you sure?",
                    text: "Once canceled, you will not be able to recover this data!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willCancel) => {
                        if (!willCancel) {
                            $('#userDetailModal').modal();
                        }
                    });
            }
        };

        function updateRoles() {
            $http.get('/api/roles').then(function(response) {
                vm.roles = response.data;
            });
        }

        $scope.$on('deliverNewUser', function(event, user) {
            vm.originalUser = JSON.parse(JSON.stringify(user));
            updateRoles();
            $scope.userForm.$setPristine();
            $scope.userForm.$setUntouched();
            vm.validations = {};
            vm.isChanged = false;

            vm.changePassword = !vm.originalUser.id;
        });
    },
    bindings: {
        user: '=',
        actionType: '='
    }
});
