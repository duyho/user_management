app.component('userComponent', {
    templateUrl: '/app/components/users/user.template.html',
    controller: function($scope, $http, config) {
        $scope.users = [];

        $http.get('/api/users').then(function(response) {
            $scope.users = response.data;
        });

        $scope.constants = config;
        $scope.actionType  = 'create';
        $scope.currentUser = {};

        $scope.formatDate = function(timestamp) {
            var date = new Date(timestamp * 1000);
            return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
        };
        $scope.openCreateUserModal = function() {
            $scope.currentUser = {};
            $scope.$broadcast('deliverNewUser', {});
            $scope.actionType = 'create';
            $('#userDetailModal').modal();
        };
        $scope.openUpdateUserModal = function(index) {
            $scope.currentUser = JSON.parse(JSON.stringify($scope.users[index]));
            $scope.$broadcast('deliverNewUser', $scope.currentUser);
            $scope.actionType = 'update';
            $('#userDetailModal').modal();
        };
        $scope.openDeleteUserPopup = function(index) {
            var user = $scope.users[index];
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this user!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $http.delete(`/api/users/${user.id}`).then(function() {
                            swal("Poof! The user has been deleted!", {
                                icon: "success",
                            });
                            $scope.users.splice(index, 1);
                        });

                    } else {
                    }
                });
        };

        $scope.$on('userCreated', function(e, data) {
            $scope.users.push(data);
        });

        $scope.$on('userUpdated', function(e, data) {
            for (var index in $scope.users) {
                if ($scope.users[index].id == data.id) {
                    $scope.users[index] = data;
                }
            }
        })
    },
    bindings: {
    }
});
