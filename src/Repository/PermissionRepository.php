<?php
/**
 * Date: 2/11/2019
 * Time: 9:58 AM
 */

namespace App\Repository;


use App\Entity\Permission;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PermissionRepository {
    private $entityManager = null;

    /**
     * UserRepository constructor.
     */
    public function __construct(EntityManagerInterface $em) {
        $this->entityManager = $em;
    }

    public function getList($page = 1, $perPage = 20) {
        $dql   = "SELECT p FROM App\Entity\Permission p WHERE p.deletedAt is null";

        $query = $this->entityManager->createQuery($dql)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        $paginator = new Paginator($query, $fetchJoinCollection = true);

        $c = count($paginator);

        $result = [];
        foreach ($paginator as $permission) {
            $result[] = $permission;
        }

        return $result;
    }

    public function getById($roleId, $getDeleted = false) {
        $permission          = $this->entityManager->getRepository(Permission::class)->find($roleId);

        // if user found and not be deleted
        if (!empty($permission) && empty($permission->getDeletedAt())) {
            return $permission;
        }

        // force to get deleted user
        if (!empty($permission) && $getDeleted) {
            return $permission;
        }

        return null;
    }

    public function create($value, $name) {
        $permission = new Permission();
        $permission->setValue($value);
        $permission->setName($name);
        $permission->setCreatedAt(new \DateTime());
        $permission->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($permission);
        $this->entityManager->flush($permission);

        return $permission;
    }

    public function update(Permission $permission, $value, $name) {
        $permission->setValue($value);
        $permission->setName($name);
        $permission->setUpdatedAt(new \DateTime());

        $this->entityManager->flush($permission);
        return $permission;
    }

    public function delete(Permission $permission) {
        $permission->setDeletedAt(new \DateTime());
        $this->entityManager->flush($permission);
    }
}