<?php
/**
 * Date: 2/11/2019
 * Time: 9:58 AM
 */

namespace App\Repository;


use App\Entity\Role;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRepository {
    private $entityManager = null;
    private $encoder       = null;

    /**
     * UserRepository constructor.
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder) {
        $this->entityManager = $em;
        $this->encoder       = $encoder;
    }

    public function getList($page = 1, $perPage = 20) {

        $dql   = "SELECT u FROM App\Entity\User u WHERE u.deletedAt is null";
        $query = $this->entityManager->createQuery($dql)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        $paginator = new Paginator($query, $fetchJoinCollection = true);

        $c = count($paginator);

        $result = [];
        foreach ($paginator as $user) {
            $result[] = $user;
        }

        return $result;
    }

    public function getById($userId, $getDeleted = false) {
        $user = $this->entityManager->getRepository(User::class)->find($userId);

        // if user found and not be deleted
        if (!empty($user) && empty($user->getDeletedAt())) {
            return $user;
        }

        // force to get deleted user
        if (!empty($user) && $getDeleted) {
            return $user;
        }

        return null;
    }

    public function create($data, Role $role) {
        $user             = new User();
        $data['password'] = $this->encoder->encodePassword($user, $data['password']);
        $user->setFromArray($data);
        $user->setRole($role);
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        return $user;
    }

    public function update(User $user, $data, Role $role) {
        $data['password'] = $this->encoder->encodePassword($user, $data['password']);
        $user->setFromArray($data);
        $user->setUpdatedAt(new \DateTime());
        $user->setRole($role);

        $this->entityManager->flush($user);
        return $user;
    }

    public function delete($user) {
        $user->setDeletedAt(new \DateTime());
        $this->entityManager->flush($user);
    }
}