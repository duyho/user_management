<?php
/**
 * Date: 2/11/2019
 * Time: 9:58 AM
 */

namespace App\Repository;


use App\Entity\Role;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class RoleRepository {
    private $entityManager = null;

    /**
     * UserRepository constructor.
     */
    public function __construct(EntityManagerInterface $em) {
        $this->entityManager = $em;
    }

    public function getList($page = 1, $perPage = 20) {
        $dql   = "SELECT r FROM App\Entity\Role r WHERE r.deletedAt is null";

        $query = $this->entityManager->createQuery($dql)
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage);

        $paginator = new Paginator($query, $fetchJoinCollection = true);

        $c = count($paginator);

        $result = [];
        foreach ($paginator as $role) {
            $result[] = $role;
        }

        return $result;
    }

    public function getById($roleId, $getDeleted = false) {
        $role          = $this->entityManager->getRepository(Role::class)->find($roleId);

        // if user found and not be deleted
        if (!empty($role) && empty($role->getDeletedAt())) {
            return $role;
        }

        // force to get deleted user
        if (!empty($role) && $getDeleted) {
            return $role;
        }

        return null;
    }

    public function create($data, $permissions) {
        $role = new Role();
        $role->setName($data['name']);
        $role->setCreatedAt(new \DateTime());
        $role->setUpdatedAt(new \DateTime());

        foreach ($permissions as $permission) {
            $role->addPermission($permission);
        }

        $this->entityManager->persist($role);
        $this->entityManager->flush($role);

        return $role;
    }

    public function update(Role $role, $data, $permissions = []) {
        $role->setName($data['name']);
        $role->setUpdatedAt(new \DateTime());

        $existingPermissions = $role->getPermissions();

        // delete permission
        foreach ($existingPermissions as $existingPermission) {
            $delete = true;
            foreach ($permissions as $permission) {
                if ($permission->getId() == $existingPermission->getId()) {
                    $delete = false;
                }
            }

            if ($delete) {
                $role->removePermission($existingPermission);
            }
        }

        // add new permission
        foreach ($permissions as $permission) {
            $add = true;
            foreach ($existingPermissions as $existingPermission) {
                if ($permission->getId() == $existingPermission->getId()) {
                    $add = false;
                }
            }

            if ($add) {
                $role->addPermission($permission);
            }
        }

        $this->entityManager->persist($role);
        $this->entityManager->flush($role);

        return $role;
    }

    public function delete(Role $role) {
        $role->setDeletedAt(new \DateTime());
        $this->entityManager->flush($role);
    }
}