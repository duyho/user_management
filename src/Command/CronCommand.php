<?php
/**
 * Date: 2/12/2019
 * Time: 4:23 PM
 */

namespace App\Command;


use App\Service\CronService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronCommand extends Command {
    protected static $defaultName = 'app:hourly-cron';
    private $cronService;
    /**
     * CronCommand constructor.
     */
    public function __construct(CronService $cronService) {
        parent::__construct();
        $this->cronService = $cronService;
    }


    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->cronService->usersReportHandler();
            $output->writeln("Cron successfully");
        }
        catch (\Exception $exception) {
            $output->writeln("Something went wrong");
            $output->writeln($exception->getMessage());
        }
        // ...
    }
}