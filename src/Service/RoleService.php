<?php
/**
 * Date: 2/11/2019
 * Time: 11:31 AM
 */

namespace App\Service;


use App\Repository\PermissionRepository;
use App\Repository\RoleRepository;
use App\UtilService;

class RoleService {
    private $roleRepository;
    private $permissionRepository;

    /**
     * RoleService constructor.
     */
    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository) {
        $this->roleRepository       = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function getList($page = 1) {
        $roles = $this->roleRepository->getList($page, 500);

        $result = [];
        foreach ($roles as $role) {
            $result[] = UtilService::getRoleData($role);
        }
        return $result;
    }

    public function getById($roleId) {
        $role = $this->roleRepository->getById($roleId);
        if (empty($role))
            throw new \Exception("Role not found");

        $result = UtilService::getRoleDataWithPermission($role);
        return $result;
    }

    public function create($data) {
        $permissions = [];

        foreach ($data['permissions'] as $permissionId) {
            $permission = $this->permissionRepository->getById($permissionId);
            if (empty($permission))
                throw new \Exception("Permission not found");
            $permissions[] = $permission;
        }
        $role = $this->roleRepository->create($data, $permissions);
        return UtilService::getRoleDataWithPermission($role);
    }

    public function update($roleId, $data) {
        $role = $this->roleRepository->getById($roleId);

        if (empty($role)) {
            throw new \Exception("Role not found");
        }

        $permissions = [];
        foreach ($data['permissions'] as $permissionId) {
            $permission = $this->permissionRepository->getById($permissionId);
            if (empty($permission))
                throw new \Exception("Permission not found");
            $permissions[] = $permission;
        }
        $role = $this->roleRepository->update($role, $data, $permissions);
        return UtilService::getRoleDataWithPermission($role);
    }

    public function delete($roleId) {
        $role = $this->roleRepository->getById($roleId);

        if (empty($role)) {
            throw new \Exception("Role not found");
        }

        $this->roleRepository->delete($role);
    }
}