<?php
/**
 * Date: 2/11/2019
 * Time: 3:09 PM
 */

namespace App\Service;


use App\Repository\PermissionRepository;
use App\UtilService;

class PermissionService {
    private $permissionRepository;

    /**
     * PermissionService constructor.
     */
    public function __construct(PermissionRepository $permissionRepository) {
        $this->permissionRepository = $permissionRepository;
    }

    public function getList($page = 1) {
        $permissions = $this->permissionRepository->getList($page, 500);

        $result = [];
        foreach ($permissions as $permission) {
            $result[] = UtilService::getPermissionData($permission);
        }
        return $result;
    }

    public function getById($permissionId) {
        $permission = $this->permissionRepository->getById($permissionId);
        if (empty($permission))
            throw new \Exception("Permission not found");

        $result = UtilService::getPermissionData($permission);
        return $result;
    }

    public function create($value, $name) {
        $permission = $this->permissionRepository->create($value, $name);
        return UtilService::getPermissionData($permission);
    }

    public function update($permissionId, $value, $name) {
        $permission = $this->permissionRepository->getById($permissionId);
        if (empty($permission))
            throw new \Exception("Permission not found");

        $permission = $this->permissionRepository->update($permission, $value, $name);
        return UtilService::getPermissionData($permission);
    }

    public function delete($permissionId) {
        $permission = $this->permissionRepository->getById($permissionId);
        if (empty($permission))
            throw new \Exception("Permission not found");

        $this->permissionRepository->delete($permission);
    }
}