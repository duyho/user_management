<?php
/**
 * Date: 1/23/2019
 * Time: 2:29 PM
 */

namespace App\Service\Soap;


use App\Entity\User;
use App\UtilService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Runner\Exception;

class UserService {
    private $entityManager;

    /**
     * UserService constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function edit($id, $username, $firstName, $lastName, $address, $phone, $gender)
    {
        try {
            $user = $this->entityManager->getRepository(User::class)->find($id);

            if (empty($user)) {
                return ['error' => 1, 'message' => 'User not found'];
            }

            if (!empty($username))
                $user->setUsername($username);

            if (!empty($firstName))
                $user->setFirstName($firstName);

            if (!empty($lastName))
                $user->setLastName($lastName);

            if (!empty($address))
                $user->setAddress($address);

            if (!empty($phone))
                $user->setPhone($phone);

            if (!empty($gender))
                $user->setGender($gender);


            $data = UtilService::getUserData($user);
            //        $this->entityManager->flush();

            return ['error' => 0, 'data' => $data];
        }
        catch (Exception $exception) {
            return ['error' => 1, 'message' => $exception->getMessage()];
        }
    }
}