<?php
/**
 * Date: 2/12/2019
 * Time: 5:31 PM
 */

namespace App\Service;


use App\Repository\UserRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class CronService {
    private $mailer;
    private $container;
    private $userRepository;

    /**
     * CronCommand constructor.
     */
    public function __construct(ContainerInterface $container, \Swift_Mailer $mailer, UserRepository $userRepository) {
        $this->mailer         = $mailer;
        $this->container      = $container;
        $this->userRepository = $userRepository;
    }

    public function usersReportHandler() {
        $filePath = self::createUserReportCSVFile();
        self::sendUsersReportEmail([$filePath]);
    }

    private function sendUsersReportEmail($attachmentFilePaths = []) {
        $message = new \Swift_Message("Hourly report at " . date('Y-m-d H:i:s'));
        $message->setFrom("itoldyouwhat@gmail.com");
        $message->setTo("nhatduy1tn@gmail.com");
        $message->setBody("<h1>Hi there,</h1> <p>This is hourly report containing a list of users</p>", 'text/html');

        foreach ($attachmentFilePaths as $attachmentFilePath) {
            $message->attach(\Swift_Attachment::fromPath($attachmentFilePath));
        }

        return $this->mailer->send($message);
    }

    private function createUserReportCSVFile() {
        $users = $this->userRepository->getList(1, 500);

        $data = [];
        foreach ($users as $user) {
            $data[] = [$user->getId(), $user->getUsername(), $user->getAddress(), $user->getPhone()];
        }

        $lines = [];
        foreach ($data as $datum) {
            $lines[] = implode(",", $datum);
        }
        $fileContent = implode(PHP_EOL, $lines);
        $fileSystem = new Filesystem();

        $filePath = $this->container->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "tmp" . DIRECTORY_SEPARATOR . date('Y_m_d_H_i_s') . ".csv";

        $fileSystem->dumpFile($filePath, $fileContent);

        return $filePath;
    }
}