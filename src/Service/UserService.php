<?php
/**
 * Date: 2/11/2019
 * Time: 9:59 AM
 */

namespace App\Service;


use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\UtilService;

class UserService {
    private $userRepository;
    private $roleRepository;

    /**
     * UserService constructor.
     */
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function getList($page = 1) {
        $users = $this->userRepository->getList($page, 500);

        $result = [];
        foreach ($users as $user) {
            $result[] = UtilService::getUserData($user);
        }
        return $result;
    }

    public function getById($userId) {
        $user = $this->userRepository->getById($userId);
        if (empty($user))
            throw new \Exception("User not found");

        $result = UtilService::getUserData($user);
        return $result;
    }

    public function create($data) {
        if (empty($roleId = $data['roleId']))
            throw new \Exception("Role ID is missing");

        $role = $this->roleRepository->getById($roleId);
        if (empty($role))
            throw new \Exception("Role not found");

        $user = $this->userRepository->create($data, $role);
        return UtilService::getUserData($user);
    }

    public function update($userId, $data) {
        if (empty($userId))
            throw new \Exception("User ID is missing");

        $user = $this->userRepository->getById($userId);
        if (empty($user))
            throw new \Exception("User not found");

        if (empty($roleId = $data['roleId']))
            throw new \Exception("Role ID is missing");

        $role = $this->roleRepository->getById($roleId);
        if (empty($role))
            throw new \Exception("Role not found");

        $user = $this->userRepository->update($user, $data, $role);
        return UtilService::getUserData($user);
    }

    public function delete($userId) {
        $user = $this->userRepository->getById($userId);
        if (empty($user))
            throw new \Exception("User not found");

        $this->userRepository->delete($user);
    }
}