<?php
/**
 * Date: 1/18/2019
 * Time: 3:31 PM
 */

namespace App;


use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;

class UtilService {
    public static function getUserData(User $user) {
        return [
            'id'        => $user->getId(),
            'username'  => $user->getUsername(),
            'firstName' => $user->getFirstName(),
            'lastName'  => $user->getLastName(),
            'address'   => $user->getAddress(),
            'phone'     => $user->getPhone(),
            'gender'    => $user->getGender(),
            'role'      => $user->getRole()->getName(),
            'roleId'    => $user->getRole()->getId(),
            'createdAt' => $user->getCreatedAt()->getTimestamp()
        ];
    }

    public static function getUserDetailData(User $user) {
        $res = self::getUserData($user);
        $permissions = $user->getRole()->getPermissions();

        $res['permissions'] = [];
        foreach ($permissions as $permission) {
            $res['permissions'][] = $permission->getValue();
        }

        return $res;
    }

    public static function getRoleData(Role $role) {
        return [
            'id'        => $role->getId(),
            'name'      => $role->getName(),
            'createdAt' => $role->getCreatedAt()->getTimestamp()
        ];
    }

    public static function getRoleDataWithPermission(Role $role) {
        $res = self::getRoleData($role);

        $permissions        = $role->getPermissions();
        $res['permissions'] = [];
        foreach ($permissions as $permission) {
            $res['permissions'][] = $permission->getId();
        }

        return $res;
    }

    public static function getPermissionData(Permission $permission) {
        return [
            'id'    => $permission->getId(),
            'value' => $permission->getValue(),
            'name'  => $permission->getName()
        ];
    }
}