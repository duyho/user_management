<?php
/**
 * Date: 1/16/2019
 * Time: 11:16 AM
 */

namespace App\Controller;


use App\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController {

    /**
     * @Route("/users", methods={"GET"})
     * @Route("/users/{page}", methods={"GET"}, requirements={"page"=".*"})
     */
    public function index() {
        $roles = $this->getDoctrine()->getManager()->getRepository(Role::class)->findAll();
        $rolesData = [];
        foreach ($roles as $role) {
            $rolesData[] = [
                'id' => $role->getId(),
                'name' => $role->getName()
            ];
        }

        return $this->render('users/index.html.twig', compact('rolesData'));
    }
}