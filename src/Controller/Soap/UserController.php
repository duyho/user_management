<?php
/**
 * Date: 1/16/2019
 * Time: 10:57 AM
 */

namespace App\Controller\Soap;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller {

    /**
     * @Route("/soap/users", methods={"POST"})
     */
    public function index() {
        ini_set("soap.wsdl_cache_enabled", "0");

        $options = array(
            'uri'        => 'http://usermanagement.localhost/soap/users',
            'cache_wsdl' => WSDL_CACHE_NONE,
            'exceptions' => true
        );

        $server = new \SoapServer(dirname(__FILE__) . '/../../../public/wsdl/users.wsdl', $options);
        $server->setObject($this->get('app.service.soap.user'));

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=utf-8');

        ob_start();
        $server->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }
}