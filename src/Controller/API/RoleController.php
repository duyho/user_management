<?php
/**
 * Date: 1/18/2019
 * Time: 3:25 PM
 */

namespace App\Controller\API;

use App\Service\RoleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RoleController extends AbstractController {
    private $roleService;

    /**
     * RoleController constructor.
     */
    public function __construct(RoleService $roleService) {
        $this->roleService = $roleService;
    }


    /**
     * @Route("/api/roles", methods={"GET"})
     */
    public function getList(Request $request) {
        try {
            if (!$this->getUser()->can('view_role')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $responseData = $this->roleService->getList();
            return new JsonResponse($responseData);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to get roles', 'error' => $exception->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/roles/{roleId}", methods={"GET"})
     */
    public function detail($roleId) {
        try {
            if (!$this->getUser()->can('view_role')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $responseData = $this->roleService->getById($roleId);

            return new JsonResponse($responseData);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to get role', 'error' => $exception->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/roles", methods={"POST"})
     */
    public function create(Request $request) {
        try {
            if (!$this->getUser()->can('create_role')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $parametersAsArray = [];
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
            }

            $responseData = $this->roleService->create($parametersAsArray);

            return new JsonResponse($responseData, 201);
        }
        catch (\Exception $e) {
            return new JsonResponse(['message' => 'Fail to create new role', 'error' => $e->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/roles/{roleId}", methods={"PUT"})
     */
    public function update(Request $request, $roleId) {
        try {
            if (!$this->getUser()->can('update_role')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $parametersAsArray = [];
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
            }

            $responseData = $this->roleService->update($roleId, $parametersAsArray);

            return new JsonResponse($responseData);
        }
        catch (\Exception $e) {
            return new JsonResponse(['message' => 'Fail to update role', 'error' => $e->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/roles/{roleId}", methods={"DELETE"})
     */
    public function delete($roleId) {
        try {
            if (!$this->getUser()->can('delete_role')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $this->roleService->delete($roleId);
            return new JsonResponse(['message' => 'Deleted successfully']);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to delete role', 'error' => $exception->getMessage()], 400);
        }
    }
}