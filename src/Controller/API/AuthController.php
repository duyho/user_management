<?php
/**
 * Date: 1/16/2019
 * Time: 10:57 AM
 */

namespace App\Controller\API;


use App\UtilService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController {

    /**
     * @Route("/api/login_check", methods={"POST"})
     */
    public function loginCheck(Request $request) {
        return true;
    }

    /**
     * @Route("/api/auth/info", methods={"GET"})
     */
    public function getInfo() {
        return new JsonResponse(UtilService::getUserDetailData($this->getUser()));
    }

}