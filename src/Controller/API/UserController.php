<?php
/**
 * Date: 1/16/2019
 * Time: 10:57 AM
 */

namespace App\Controller\API;


use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController {
    private $userService;

    /**
     * UserController constructor.
     */
    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * @Route("/api/users", name="api_get_users", methods={"GET"})
     */
    public function getList(Request $request) {
        if (!$this->getUser()->can('view_user')) {
            return new JsonResponse(['message' => 'Access is denied'], 403);
        }


        $responseData = $this->userService->getList();
        return new JsonResponse($responseData);
    }

    /**
     * @Route("/api/users/{userId}", name="api_get_user_detail", methods={"GET"})
     */
    public function detail($userId) {
        try {
            if (!$this->getUser()->can('view_user')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $responseData = $this->userService->getById($userId);
            return new JsonResponse($responseData);
        }
        catch (\Exception $e) {
            return new JsonResponse(['message' => 'Fail to get user', 'error' => $e->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/users", name="api_create_user", methods={"POST"})
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder) {
        try {
            if (!$this->getUser()->can('create_user')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $parametersAsArray = [];
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
            }

            $responseData = $this->userService->create($parametersAsArray);

            return new JsonResponse($responseData, 201);
        }
        catch (\Exception $e) {
            return new JsonResponse(['message' => 'Fail to create new user', 'error' => $e->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/users/{userId}", name="api_update_user", methods={"PUT"})
     */
    public function update(Request $request, $userId, UserPasswordEncoderInterface $encoder) {
        try {
            if (!$this->getUser()->can('update_user')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $parametersAsArray = [];
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
            }

            $responseData = $this->userService->update($userId, $parametersAsArray);

            return new JsonResponse($responseData);
        }
        catch (\Exception $e) {
            return new JsonResponse(['message' => 'Fail to update user', 'error' => $e->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/users/{userId}", name="api_delete_user", methods={"DELETE"})
     */
    public function delete($userId) {
        try {
            if (!$this->getUser()->can('delete_user')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $this->userService->delete($userId);
            return new JsonResponse(['message' => 'Deleted successfully']);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to delete user', 'error' => $exception->getMessage()], 400);
        }
    }
}