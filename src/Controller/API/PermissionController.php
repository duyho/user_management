<?php
/**
 * Date: 1/18/2019
 * Time: 3:25 PM
 */

namespace App\Controller\API;

use App\Service\PermissionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PermissionController extends AbstractController {
    private $permissionService;

    /**
     * PermissionController constructor.
     * @param $permissionService
     */
    public function __construct(PermissionService $permissionService) {
        $this->permissionService = $permissionService;
    }

    /**
     * @Route("/api/permissions", methods={"GET"})
     */
    public function getList(Request $request) {
        try {
            if (!$this->getUser()->can('view_permission')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $responseData = $this->permissionService->getList();
            return new JsonResponse($responseData);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to get permissions', 'error' => $exception->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/permissions/{permissionId}", methods={"GET"})
     */
    public function detail($permissionId) {
        try {
            if (!$this->getUser()->can('view_permission')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $responseData = $this->permissionService->getById($permissionId);
            return new JsonResponse($responseData);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to get permission', 'error' => $exception->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/permissions", methods={"POST"})
     */
    public function create(Request $request) {
        try {
            if (!$this->getUser()->can('create_permission')) {
                return new JsonResponse(['message' => 'Access is denied'],403);
            }

            $parametersAsArray = [];
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
            }

            $responseData = $this->permissionService->create($parametersAsArray['value'], $parametersAsArray['name']);
            return new JsonResponse($responseData, 201);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to create permission', 'error' => $exception->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/permissions/{permissionId}", methods={"PUT"})
     */
    public function update(Request $request, $permissionId) {
        try {
            if (!$this->getUser()->can('update_permission')) {
                return new JsonResponse(['message' => 'Access is denied'],403);
            }

            $parametersAsArray = [];
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
            }

            $responseData = $this->permissionService->update($permissionId, $parametersAsArray['value'], $parametersAsArray['name']);
            return new JsonResponse($responseData, 200);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to update permission', 'error' => $exception->getMessage()], 400);
        }
    }

    /**
     * @Route("/api/permissions/{permissionId}", methods={"DELETE"})
     */
    public function delete($permissionId) {
        try {
            if (!$this->getUser()->can('delete_permission')) {
                return new JsonResponse(['message' => 'Access is denied'], 403);
            }

            $this->permissionService->delete($permissionId);
            return new JsonResponse(['message' => 'Deleted successfully']);
        }
        catch (\Exception $exception) {
            return new JsonResponse(['message' => 'Fail to delete permission', 'error' => $exception->getMessage()], 400);
        }
    }
}