<?php
/**
 * Date: 1/17/2019
 * Time: 4:49 PM
 */
namespace App\Tests\Features\APIs;

use App\Entity\User;

class AuthTest extends BaseTestCase {
    /** @test */
    public function canGetInfo() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);
        $client->request('GET', '/api/auth/info', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getContent());
        $jsonResponse = json_decode($response->getContent(), true);

        $this->assertEquals($this->user1->getUsername(), @$jsonResponse['username']);
        $this->assertEquals($this->user1->getFirstName(), @$jsonResponse['firstName']);
        $this->assertEquals($this->user1->getLastName(), @$jsonResponse['lastName']);
        $this->assertEquals($this->user1->getAddress(), @$jsonResponse['address']);
        $this->assertEquals($this->user1->getPhone(), @$jsonResponse['phone']);
        $this->assertEquals($this->user1->getGender(), @$jsonResponse['gender']);

        $actualPermissionIds = [];
        foreach ($this->user1->getRole()->getPermissions() as $permission) {
            $actualPermissionIds[] = $permission->getValue();
        }
        $this->assertJsonStringEqualsJsonString(json_encode($jsonResponse['permissions']), json_encode($actualPermissionIds));
    }
}
