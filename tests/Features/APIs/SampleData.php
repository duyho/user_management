<?php
/**
 * Date: 1/22/2019
 * Time: 20:57
 */

namespace App\Tests\Features\APIs;


use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;

trait SampleData {
    protected $roleGuest;
    protected $roleCallCenter;
    protected $roleAdmin;
    protected $user1;
    protected $user2;

    protected $permissionCanViewUser;
    protected $permissionCanCreateUser;
    protected $permissionCanUpdateUser;
    protected $permissionCanDeleteUser;

    protected $permissionCanViewRole;
    protected $permissionCanCreateRole;
    protected $permissionCanUpdateRole;
    protected $permissionCanDeleteRole;

    public function initSampleData() {
        $doctrine = $this->getApplication()->getKernel()->getContainer()->get('doctrine');
        $doctrine->resetManager();
        $em      = $doctrine->getManager();
        $encoder = $this->getApplication()->getKernel()->getContainer()->get('security.password_encoder');

        $this->initPermissionData($em);
        $this->initRoleData($em);
        $this->initUserData($em, $encoder);
    }

    public function createToken($username, $client = null) {
        $data = array('username' => $username, 'roles' => ['USER']);

        if (empty($client)) {
            $client = $this->getApplication()->getKernel();
        }
        return $client->getContainer()
            ->get('lexik_jwt_authentication.encoder')
            ->encode($data);
    }

    public function initPermissionData($em) {
        $this->initUserPermissionData($em);
        $this->initRolePermissionData($em);
    }

    public function initUserPermissionData($em) {
        $this->permissionCanViewUser = new Permission();
        $this->permissionCanViewUser->setName('View user');
        $this->permissionCanViewUser->setValue('view_user');
        $this->permissionCanViewUser->setCreatedAt(new \DateTime());
        $this->permissionCanViewUser->setUpdatedAt(new \DateTime());

        $this->permissionCanCreateUser = new Permission();
        $this->permissionCanCreateUser->setName('Create user');
        $this->permissionCanCreateUser->setValue('create_user');
        $this->permissionCanCreateUser->setCreatedAt(new \DateTime());
        $this->permissionCanCreateUser->setUpdatedAt(new \DateTime());

        $this->permissionCanUpdateUser = new Permission();
        $this->permissionCanUpdateUser->setName('Update user');
        $this->permissionCanUpdateUser->setValue('update_user');
        $this->permissionCanUpdateUser->setCreatedAt(new \DateTime());
        $this->permissionCanUpdateUser->setUpdatedAt(new \DateTime());

        $this->permissionCanDeleteUser = new Permission();
        $this->permissionCanDeleteUser->setName('Delete user');
        $this->permissionCanDeleteUser->setValue('delete_user');
        $this->permissionCanDeleteUser->setCreatedAt(new \DateTime());
        $this->permissionCanDeleteUser->setUpdatedAt(new \DateTime());

        $em->persist($this->permissionCanViewUser);
        $em->persist($this->permissionCanCreateUser);
        $em->persist($this->permissionCanUpdateUser);
        $em->persist($this->permissionCanDeleteUser);
        $em->flush();
    }

    public function initRolePermissionData($em) {
        $this->permissionCanViewRole = new Permission();
        $this->permissionCanViewRole->setName('View role');
        $this->permissionCanViewRole->setValue('view_role');
        $this->permissionCanViewRole->setCreatedAt(new \DateTime());
        $this->permissionCanViewRole->setUpdatedAt(new \DateTime());

        $this->permissionCanCreateRole = new Permission();
        $this->permissionCanCreateRole->setName('Create role');
        $this->permissionCanCreateRole->setValue('create_role');
        $this->permissionCanCreateRole->setCreatedAt(new \DateTime());
        $this->permissionCanCreateRole->setUpdatedAt(new \DateTime());

        $this->permissionCanUpdateRole = new Permission();
        $this->permissionCanUpdateRole->setName('Update role');
        $this->permissionCanUpdateRole->setValue('update_role');
        $this->permissionCanUpdateRole->setCreatedAt(new \DateTime());
        $this->permissionCanUpdateRole->setUpdatedAt(new \DateTime());

        $this->permissionCanDeleteRole = new Permission();
        $this->permissionCanDeleteRole->setName('Delete role');
        $this->permissionCanDeleteRole->setValue('delete_role');
        $this->permissionCanDeleteRole->setCreatedAt(new \DateTime());
        $this->permissionCanDeleteRole->setUpdatedAt(new \DateTime());

        $em->persist($this->permissionCanViewRole);
        $em->persist($this->permissionCanCreateRole);
        $em->persist($this->permissionCanUpdateRole);
        $em->persist($this->permissionCanDeleteRole);
        $em->flush();
    }

    public function initRoleData($em) {
        $this->roleGuest = new Role();
        $this->roleGuest->setName('Guest');
        $this->roleGuest->addPermission($this->permissionCanViewUser);
        $this->roleGuest->setCreatedAt(new \DateTime());
        $this->roleGuest->setUpdatedAt(new \DateTime());

        $this->roleCallCenter = new Role();
        $this->roleCallCenter->setName('Call Center');
        $this->roleCallCenter->addPermission($this->permissionCanViewUser);
        $this->roleCallCenter->addPermission($this->permissionCanUpdateUser);
        $this->roleCallCenter->setCreatedAt(new \DateTime());
        $this->roleCallCenter->setUpdatedAt(new \DateTime());

        $this->roleAdmin = new Role();
        $this->roleAdmin->setName('Admin');
        $this->roleAdmin->addPermission($this->permissionCanViewUser);
        $this->roleAdmin->addPermission($this->permissionCanCreateUser);
        $this->roleAdmin->addPermission($this->permissionCanUpdateUser);
        $this->roleAdmin->addPermission($this->permissionCanDeleteUser);
        $this->roleAdmin->addPermission($this->permissionCanViewRole);
        $this->roleAdmin->addPermission($this->permissionCanCreateRole);
        $this->roleAdmin->addPermission($this->permissionCanUpdateRole);
        $this->roleAdmin->addPermission($this->permissionCanDeleteRole);
        $this->roleAdmin->setCreatedAt(new \DateTime());
        $this->roleAdmin->setUpdatedAt(new \DateTime());

        $em->persist($this->roleGuest);
        $em->persist($this->roleCallCenter);
        $em->persist($this->roleAdmin);
        $em->flush();
    }

    public function initUserData($em, $encoder) {
        $this->user1 = new User();
        $this->user1->setUsername('nickyminaij');
        $this->user1->setPassword($encoder->encodePassword($this->user1, '123'));
        $this->user1->setFirstName('Nicky');
        $this->user1->setLastName('Minaij');
        $this->user1->setAddress('US');
        $this->user1->setPhone('0123456789');
        $this->user1->setGender(User::GENDER_FEMALE);
        $this->user1->setRole($this->roleAdmin);
        $this->user1->setCreatedAt(new \DateTime());
        $this->user1->setUpdatedAt(new \DateTime());

        $this->user2 = new User();
        $this->user2->setUsername('tomcruise');
        $this->user2->setPassword($encoder->encodePassword($this->user2, '123'));
        $this->user2->setFirstName('Tom');
        $this->user2->setLastName('Cruise');
        $this->user2->setAddress('UK');
        $this->user2->setPhone('987654321321');
        $this->user2->setGender(User::GENDER_MALE);
        $this->user2->setRole($this->roleGuest);
        $this->user2->setCreatedAt(new \DateTime());
        $this->user2->setUpdatedAt(new \DateTime());

        $em->persist($this->user1);
        $em->persist($this->user2);
        $em->flush();
    }
}
