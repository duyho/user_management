<?php
/**
 * Date: 1/17/2019
 * Time: 4:49 PM
 */

namespace App\Tests\Features\APIs;

use App\Entity\Role;
use App\Entity\User;

class RoleTest extends BaseTestCase {
    /** @test */
    public function canGetListRoles() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('GET', '/api/roles', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $expectedResult = [
            [
                'id'        => $this->roleGuest->getId(),
                'name'      => $this->roleGuest->getName(),
                'createdAt' => $this->roleGuest->getCreatedAt()->getTimestamp()
            ],
            [
                'id'        => $this->roleCallCenter->getId(),
                'name'      => $this->roleCallCenter->getName(),
                'createdAt' => $this->roleCallCenter->getCreatedAt()->getTimestamp()
            ],
            [
                'id'        => $this->roleAdmin->getId(),
                'name'      => $this->roleAdmin->getName(),
                'createdAt' => $this->roleAdmin->getCreatedAt()->getTimestamp()
            ]

        ];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));
    }

    /** @test */
    public function canGetSpecificRole() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('GET', '/api/roles/' . $this->roleGuest->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $expectedResult = [
            'id'        => $this->roleGuest->getId(),
            'name'      => $this->roleGuest->getName(),
            'createdAt' => $this->roleGuest->getCreatedAt()->getTimestamp(),
            'permissions' => []
        ];

        foreach ($this->roleGuest->getPermissions() as $permission) {
            $expectedResult['permissions'][] = $permission->getId();
        }

        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));
    }

    /** @test */
    public function canNotGetUnknownRole() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('GET', '/api/roles/999', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResult = ['message' => 'Fail to get role', 'error' => 'Role not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));
    }

    /** @test */
    public function canCreateRole() {
        $client = static::createClient();

        $data = [
            'name'      => 'Manager',
            'permissions'    => [
                $this->permissionCanViewUser->getId(),
                $this->permissionCanUpdateUser->getId(),
                $this->permissionCanViewRole->getId(),
                $this->permissionCanUpdateRole->getId(),
            ]
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);

        $client->request('POST', '/api/roles', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertJson($response->getContent());
    }

    /** @test */
    public function canNotCreateRoleWithInvalidPermission() {
        $client = static::createClient();

        $data = [
            'name'      => 'Manager',
            'permissions'    => [
                $this->permissionCanViewUser->getId(),
                $this->permissionCanUpdateUser->getId(),
                $this->permissionCanViewRole->getId(),
                9999
            ]
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('POST', '/api/roles', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to create new role', 'error' => 'Permission not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }

    /** @test */
    public function canUpdateRole() {
        $client = static::createClient();

        $data = [
            'name' => 'Admin 1',
            'permissions'    => [
                $this->permissionCanViewUser->getId(),
                $this->permissionCanUpdateUser->getId(),
            ]
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('PUT', '/api/roles/' . $this->roleAdmin->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $em   = $client->getContainer()->get('doctrine')->getManager();
        $role = $em->getRepository(Role::class)->find($this->roleAdmin->getId());
        $em->refresh($role);
        $parsedResponse = json_decode($response->getContent(), true);
        $this->assertEquals($data['name'], @$parsedResponse['name']);
        $this->assertEquals($data['permissions'], @$parsedResponse['permissions']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getContent());
    }

    /** @test */
    public function canNotUpdateUnknownRole() {
        $client = static::createClient();

        $data = [
            'name' => 'Admin 1',
            'permissions'    => [
                $this->permissionCanViewUser->getId(),
                $this->permissionCanUpdateUser->getId(),
            ]
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('PUT', '/api/roles/999', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to update role', 'error' => 'Role not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }

    /** @test */
    public function canNotUpdateRoleWithInvalidPermission() {
        $client = static::createClient();

        $data = [
            'name'      => 'Manager',
            'permissions'    => [
                $this->permissionCanViewUser->getId(),
                $this->permissionCanUpdateUser->getId(),
                $this->permissionCanViewRole->getId(),
                9999
            ]
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('PUT', '/api/roles/' . $this->roleAdmin->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to update role', 'error' => 'Permission not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }

    /** @test */
    public function canDeleteRole() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('DELETE', '/api/roles/' . $this->roleAdmin->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $expectedResult = [
            'message' => 'Deleted successfully'
        ];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));

        $em   = $client->getContainer()->get('doctrine')->getManager();
        $role = $em->getRepository(Role::class)->find($this->roleAdmin->getId());
        $em->refresh($role);
        $this->assertNotNull($role->getDeletedAt());
    }

    /** @test */
    public function canNotDeleteUnknownRole() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('DELETE', '/api/roles/999', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to delete role', 'error' => 'Role not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }
}
