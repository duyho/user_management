<?php
/**
 * Date: 1/23/2019
 * Time: 00:08
 */

namespace App\Tests\Features\APIs;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class BaseTestCase extends WebTestCase {
    protected static $application;
    use SampleData;

    protected function setUp() {
        self::runCommand('doctrine:schema:update --force');
        $this->initSampleData();
    }

    protected static function runCommand($command) {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication() {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    protected function tearDown() {
        self::runCommand('doctrine:schema:drop --force');
    }

}
