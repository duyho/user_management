<?php
/**
 * Date: 1/17/2019
 * Time: 4:49 PM
 */
namespace App\Tests\Features\APIs;

use App\Entity\User;

class UserTest extends BaseTestCase {
    /** @test */
    public function canGetListUsers() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('GET', '/api/users', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $expectedResult = [
            [
                'id'        => $this->user1->getId(),
                'username'  => $this->user1->getUsername(),
                'firstName' => $this->user1->getFirstName(),
                'lastName'  => $this->user1->getLastName(),
                'address'   => $this->user1->getAddress(),
                'phone'     => $this->user1->getPhone(),
                'gender'    => $this->user1->getGender(),
                'role'      => $this->user1->getRole()->getName(),
                'roleId'    => $this->user1->getRole()->getId(),
                'createdAt' => $this->user1->getCreatedAt()->getTimestamp()
            ],
            [
                'id'        => $this->user2->getId(),
                'username'  => $this->user2->getUsername(),
                'firstName' => $this->user2->getFirstName(),
                'lastName'  => $this->user2->getLastName(),
                'address'   => $this->user2->getAddress(),
                'phone'     => $this->user2->getPhone(),
                'gender'    => $this->user2->getGender(),
                'role'      => $this->user2->getRole()->getName(),
                'roleId'    => $this->user2->getRole()->getId(),
                'createdAt' => $this->user2->getCreatedAt()->getTimestamp()
            ]
        ];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));
    }

    /** @test */
    public function canGetSpecificUser() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('GET', '/api/users/' . $this->user1->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();


        $this->assertEquals(200, $response->getStatusCode());

        $expectedResult = [
            'id'        => $this->user1->getId(),
            'username'  => $this->user1->getUsername(),
            'firstName' => $this->user1->getFirstName(),
            'lastName'  => $this->user1->getLastName(),
            'address'   => $this->user1->getAddress(),
            'phone'     => $this->user1->getPhone(),
            'gender'    => $this->user1->getGender(),
            'role'      => $this->user1->getRole()->getName(),
            'roleId'    => $this->user1->getRole()->getId(),
            'createdAt' => $this->user1->getCreatedAt()->getTimestamp()
        ];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));
    }

    /** @test */
    public function canNotGetUnknownUser() {
        $client = static::createClient();
        $token  = $this->createToken($this->user1->getUsername(), $client);

        $client->request('GET', '/api/users/999', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResult = ['message' => 'Fail to get user', 'error' => 'User not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));
    }

    /** @test */
    public function canCreateUser() {
        $client = static::createClient();

        $data = [
            'username'  => 'oliverqueen',
            'password'  => '123',
            'firstName' => 'Oliver',
            'lastName'  => 'Queen',
            'address'   => 'USA',
            'phone'     => '1234567891',
            'gender'    => User::GENDER_MALE,
            'roleId'    => $this->roleAdmin->getId()
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);

        $client->request('POST', '/api/users', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertJson($response->getContent());
    }

    /** @test */
    public function canNotCreateUserWithInvalidRole() {
        $client = static::createClient();

        $data = [
            'firstName' => 'Oliver',
            'lastName'  => 'Queen',
            'address'   => 'USA',
            'phone'     => '1234567891',
            'gender'    => User::GENDER_MALE,
            'roleId'    => 999
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('POST', '/api/users', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to create new user', 'error' => 'Role not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }

    /** @test */
    public function canUpdateUser() {
        $client = static::createClient();

        $data = [
            'username'  => 'nicky1minaij1',
            'password'  => '123',
            'firstName' => 'Nicky 1',
            'lastName'  => 'Minaij 1',
            'address'   => 'US 1',
            'phone'     => '1234567891999',
            'gender'    => User::GENDER_MALE,
            'roleId'    => $this->roleGuest->getId()
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('PUT', '/api/users/' . $this->user1->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $em   = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();
        $user = $em->getRepository(User::class)->find($this->user1->getId());
        $em->refresh($user);
        $parsedResponse = json_decode($response->getContent(), true);
        $this->assertEquals($data['firstName'], @$parsedResponse['firstName']);
        $this->assertEquals($data['username'], @$parsedResponse['username']);
        $this->assertEquals($data['lastName'], @$parsedResponse['lastName']);
        $this->assertEquals($data['address'], @$parsedResponse['address']);
        $this->assertEquals($data['phone'], @$parsedResponse['phone']);
        $this->assertEquals($data['gender'], @$parsedResponse['gender']);
        $this->assertEquals($data['roleId'], @$parsedResponse['roleId']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getContent());
    }

    /** @test */
    public function canNotUpdateUnknownUser() {
        $client = static::createClient();

        $data = [
            'firstName' => 'Nicky 1',
            'lastName'  => 'Minaij 1',
            'address'   => 'US 1',
            'phone'     => '1234567891999',
            'gender'    => User::GENDER_MALE,
            'roleId'    => $this->roleGuest->getId()
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('PUT', '/api/users/9', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to update user', 'error' => 'User not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }

    /** @test */
    public function canNotUpdateUserWithInvalidRole() {
        $client = static::createClient();

        $data = [
            'firstName' => 'Nicky 1',
            'lastName'  => 'Minaij 1',
            'address'   => 'US 1',
            'phone'     => '1234567891999',
            'gender'    => User::GENDER_MALE,
            'roleId'    => 999
        ];

        $token = $this->createToken($this->user1->getUsername(), $client);
        $client->request('PUT', '/api/users/' . $this->user1->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"], json_encode($data));
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to update user', 'error' => 'Role not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }

    /** @test */
    public function canDeleteUser() {
        $client = static::createClient();
        $token = $this->createToken($this->user1->getUsername(), $client);

        $client->request('DELETE', '/api/users/' . $this->user1->getId(), [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $expectedResult = [
            'message' => 'Deleted successfully'
        ];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResult));

        $em   = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();
        $user = $em->getRepository(User::class)->find($this->user1->getId());
        $em->refresh($user);
        $this->assertNotNull($user->getDeletedAt());
    }

    /** @test */
    public function canNotDeleteUnknownUser() {
        $client = static::createClient();
        $token = $this->createToken($this->user1->getUsername(), $client);

        $client->request('DELETE', '/api/users/999', [], [], ['HTTP_AUTHORIZATION' => "Bearer $token"]);
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $expectedResponse = ['message' => 'Fail to delete user', 'error' => 'User not found'];
        $this->assertJsonStringEqualsJsonString($response->getContent(), json_encode($expectedResponse));
    }
}
